'''day-1-calorie-counter.py

Given input in the following format, find the value of the most calories
in an entry.

An entry consists of one of more lines. Each line contains a single
integer value.

For example:
```
1000
2000
3000
```
The above is a single entry.

And the below is multiple entries:
```
5000
6000

7000
8000
9000

10000
```
'''

import re
from collections.abc import Callable


def evaluate(tokens: list[str], fn: Callable) -> int:
    largest_value = 0
    elf_value = 0
    for token in fn(tokens):
        try:
            # Should fail if token is empty or a newline.
            elf_value += int(token)
        except:
            # Copy our elf entry, and reset the value.
            if elf_value > largest_value:
                largest_value = elf_value
            elf_value = 0
            pass
    return largest_value


'''Answer 1 - First(ish) pass.'''
# This is the meat of the problem here. IE, how do we logically split up the
# input in such a way that we can parse and munge it effectively?
tokenize1 = lambda x : x.split('\n')

def answer_one(s: str) -> str:
    return evaluate(s, tokenize1)


'''Answer 2 - Pure regex.

We have options here. We can match all the way down to individual characters,
or as large as the entire document.

Lines are made up of one or more digits and a newline.

Entries are made up of one or more lines and a newline.

The manifest is made up of one or more entries.

We need to do math at the entry level, which means the largest hunk of text we
can match against logically is a line. We'll either match an entry line or a
newline in the case where an entry ends.

You'll see this implementation begins to approach a recursive descent parser.

:D
'''
# lexemes
DIGIT = '\d'
NEWLINE = '\n'

# tokens
## LINE --> 1000\n, where the newline is optional for the last line in the
## file.
ENTRY_LINE = '(' + DIGIT + '+' + ')' + NEWLINE + '?'

# I could do the following operation with a for loop, but I'm goint to
# let the regex engine work for me. The newline matches will be empty and
# so we'll use logic similar to answer 1 to finish everything up.
#
# All we've done here is replace Answer 1's tokenizer function.
tokenize2 = lambda x : re.findall(ENTRY_LINE + '|' + NEWLINE, x)

def answer_two(s: str) -> int:
    return evaluate(s, tokenize2)


'''Answer 3 - Full recursive descent parser.

Here we delve into "top down" parsing, where we begin at the "structure" level and
we end at the "character" or lexeme level. You'll see some terms here that you saw
above; lexemes and tokens.
'''
# lexemes - these are defined in Answer 2 and we'll loosely redefine them here.
# isDigit - Characters have a built in method isDigit that we will use. For the 
# sake of consistency, we'll alias that function.
is_digit = lambda x : x.isdigit()
is_newline = lambda x : x == NEWLINE

# tokens
def parse_value(s: str, needle: int) -> (str, int):
    '''We take the whole string as input as well as a "needle" value that
    points to the index in s where we should start reading from.
    
    We need to return two values: the value we found and a new value for
    'needle'.

    When we find nothing we'll return None and the original value of needle
    that was passed as an argument.
    '''
    if needle > len(s) - 1:
        return None, needle 

    _return_value = None
    _read_idx = needle

    while _read_idx < len(s) and is_digit(s[_read_idx]):
        if _return_value == None:
            _return_value = ''
        _return_value += s[_read_idx]
        _read_idx += 1

    return _return_value, _read_idx

def parse_line(s: str, needle: int) -> (str, int):
    '''We expect to read a value AND an optional newline OR read a single
    newline. The optional newline is there to account for an entry at the end
    of a manifest that may or may not have a newline after it.

    If we find a value AND maybe a newline, we return that value and _read_idx.
    
    Else if we find a newline we return NEWLINE and _read_idx.
    '''
    _return = None
    _found_value, _read_idx = parse_value(s, needle)

    # If parse_value succeeds, check for a newline.
    if _found_value is not None:
        _return = _found_value
    
    # Check for a newline and adjust _read_idx accordingly.
    try:
        if s[_read_idx] == '\n':
            _read_idx = _read_idx + 1
            # If we only found a newline and no value, return a newline.
            if _found_value is None:
                return NEWLINE, _read_idx
    except IndexError():
        return None, _read_idx

    return _return, _read_idx

def parse_entry(s: str, needle: int) -> list[str]:
    '''An entry is one or more lines.

    While we find lines containing values, add them to a list to be returned.

    When parse_line returns NEWLINE, we've found a newline by itself; return the
    list.
    '''
    _return = None
    _list_entry, _read_idx = parse_line(s, needle)
    while _list_entry is not NEWLINE:
        if _return is None:
            _return = []
        _return.append(_list_entry)
        if _read_idx < len(s) - 1:
            _list_entry, _read_idx = parse_line(s, _read_idx)
        else:
            break
    return _return, _read_idx

def parse_manifest(s: str) -> int:
    '''What I call a 'manifest' is a collection of one or more entries.

    While we find entries, add them to a list to be returned.

    When parse_entry returns None, return the list.
    '''
    _return_value = None
    _read_idx = 0
    _entry_list, _read_idx = parse_entry(s, _read_idx)
    while _entry_list is not None:
        if _return_value == None:
            _return_value = []
        _return_value.append(_entry_list)
        if _read_idx < len(s) - 1:
            _entry_list, _read_idx = parse_entry(s, _read_idx)
        else:
            break

    return _return_value

def answer_three(s: str) -> int:
    # Parse the kcal manifest.
    manifest = parse_manifest(s)
    # Do some math on the resulting AST.
    _out = 0
    for entry in manifest:
        _val = 0
        for line in entry:
            _val += int(line)
        if _val > _out:
            _out = _val
    
    return _out

def answer_four(s: str) -> int:
    manifest = parse_manifest(s)
    _manifest_sums = []
    for entry in manifest:
        _val = 0
        for line in entry:
            _val += int(line)
        _manifest_sums.append(_val)

    _manifest_sums.sort()
    _manifest_sums.reverse()

    print(_manifest_sums[0:3])
    return sum(_manifest_sums[0:3])

_other_input = '''1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
'''

def main():
    with open('day-1-puzzle-input.txt') as _f:
        _input = _f.read()

        # Part One
        print(answer_one(_input))
        print(answer_two(_input))
        print(answer_three(_input))
        
        # Part Two
        print(answer_four(_input))

if __name__ == '__main__':
    main()